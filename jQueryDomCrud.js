$(document).ready(function() {
    // alert("ready")
    // Create a new <a> element containing the text "Buy Now!"
    // with an id of "cta" after the last <p>

    $("main").append("<a href=\"#\" id=\"cta\">Buy Now!</a>");


    // Access (read) the data-color attribute of the <img>,
    // log to the console

    const $img = $("img");
    console.log ("data-color:", $img.data('color'));


    // Update the third <li> item ("Turbocharged"),
    // set the class name to "highlight"

    const $li = $('li')[2];
    $($li).addClass('highlight');

    // Remove (delete) the last paragraph
    // (starts with "Available for purchase now…")
    // $main.$("p").remove()
    $("main").find("p").remove()


    // Create a listener on the "Buy Now!" link that responds to a click event.
    // When clicked, the the "Buy Now!" link should be removed
    // and replaced with text that says "Added to cart"

    $( "#cta" ).click(function() {
        this.remove()
        $("main").append("<p>Added to cart</p>");
    });
});

