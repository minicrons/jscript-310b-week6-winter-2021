/**
 * Car class
 * @constructor
 * @param {String} model
 */

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()


class Car {
    constructor(model) {
        this.currentSpeed = 0;
        this.model = model;
    }
    accelerate() {this.currentSpeed ++;}
    decreases() {this.currentSpeed --;}
    toString() {
        console.log(`${this.model}'s current speed is ${this.currentSpeed}`);
    } 
}

let car = new Car("Civic");
car.accelerate()
car.accelerate()
car.decreases()
car.toString()

/**
 * ElectricCar class
 * @constructor
 * @param {String} model
 */

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()

class ElectricCar extends Car {
    constructor(model) {
        super(model)
        this.motor = "electric";
    }
    accelerate () {
        super.accelerate();
        super.accelerate();
    }
    toString() {
        console.log(`${this.model}'s current speed is ${this.currentSpeed}; it's an ${this.motor} car.`);
    }
}
let eCar = new ElectricCar("Tesla");
eCar.accelerate()
eCar.accelerate()
eCar.decreases()
eCar.toString()

